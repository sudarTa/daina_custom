<?php
$edit_profile_pageurl = get('pagetitle');
?>
<div class="line">
	<div <?php if ($edit_profile_pageurl == "my_survey") { ?>class="dsp_tab1-active" <?php } else { ?>class="dsp_tab1" <?php } ?>>
	        <a href="<?php echo $root_link . "love_survey/my_survey/"; ?>"><?php echo __('Survey Question', 'wpdating'); ?></a>
	</div>
	<div <?php if ($edit_profile_pageurl == "my_survey_matches") { ?>class="dsp_tab1-active" <?php } else { ?>class="dsp_tab1" <?php } ?>>
	        <a href="<?php echo $root_link . "love_survey/my_survey_matches/"; ?>"><?php echo __('Survey Question', 'wpdating'); ?></a>
	</div>
</div>
</div>
<?php
if ($edit_profile_pageurl == "my_survey") {
    include_once(WP_DSP_ABSPATH . "members/loggedin/love_survey/edit_love_survey.php");
} else if ($edit_profile_pageurl == "my_survey_matches") {
    include_once(WP_DSP_ABSPATH . "edit_partner_profile_setup.php");
} else {
	include_once(WP_DSP_ABSPATH . "members/loggedin/love_survey/edit_love_survey.php");
}
?>