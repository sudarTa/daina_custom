<?php 
    $freeze_table = $wpdb->prefix.'payment_freeze';
	
	$action 	= $_REQUEST['Action'];
    $payment_id = isset($_REQUEST['payment_id']) ? $_REQUEST['payment_id'] : '';
    if ($_REQUEST['Action'] == 'freeze') {
    	$payment_row = $wpdb->get_row("SELECT * FROM $dsp_payments_table WHERE payment_id=$payment_id");
	    $expire = date_create($payment_row->expiration_date);
	    $now    = date_create(date("Y-m-d"));

	    $remaining_days 		= date_diff($expire,$now);

	    $data['payment_id']		= $payment_id;
	    $data['remaining_days'] = $remaining_days->days;

	    $update = $wpdb->insert($freeze_table,$data);
	 	if ($update) {
	 		$payment_update = $wpdb->update($dsp_payments_table,
	 			array('expiration_date'=>NULL),
	 			array('payment_id' => $payment_id ));
	 	}
    } else if ($_REQUEST['Action'] == 'unfreeze'){
    	$freeze_row = $wpdb->get_row("SELECT * FROM $freeze_table WHERE payment_id = $payment_id");
    	$payment_date = date("Y-m-d");

    	$updated = $wpdb->query("UPDATE $dsp_payments_table SET expiration_date = DATE_ADD('$payment_date', INTERVAL $freeze_row->remaining_days DAY) WHERE payment_id = $payment_id");
    	if($updated){
    		$deleted = $wpdb->delete($freeze_table,array('payment_id' => $payment_id ));
    	}
    }
    
?>