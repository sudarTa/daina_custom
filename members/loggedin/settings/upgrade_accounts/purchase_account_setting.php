<div class=" "><div class="box-border"><div class="box-pedding clearfix">

            <?php
            /*
              Copyright (C) www.wpdating.com - All Rights Reserved!
              Author - www.wpdating.com
              WordPress Dating Plugin
              contact@wpdating.com
             */
            //-------------------------------START UPGRADE ACCOUNT SETTINGS ----------------------------------
            global $wpdb;
            $bloginfo_keys = array('admin_email', 'description', 'name', 'url', 'wpurl');
            $blogInfo      = array();
            foreach ($bloginfo_keys as $bloginfo_key) {
                $blogInfo[$bloginfo_key] = get_bloginfo($bloginfo_key);
            }
            $uploadInfo = wp_upload_dir();

            $upgrade_mode           = isset($_REQUEST['mode']) ? $_REQUEST['mode'] : get('mode');
            $membership_plan        = isset($_REQUEST['item_name']) ? $_REQUEST['item_name'] : '';
            $membership_plan_id     = isset($_REQUEST['membership_id']) ? $_REQUEST['membership_id'] : '';
            $membership_plan_amount = isset($_REQUEST['amount']) ? $_REQUEST['amount'] : '';
            $discount_code          = isset($_REQUEST['code']) ? $_REQUEST['code'] : '';
            $dateTimeFormat         = dsp_get_date_timezone();
            extract($dateTimeFormat);
            $payment_date = date("Y-m-d");
            // get the subscription detail from db

            if (($upgrade_mode == 'purchase') && $membership_plan_id != "") {
                $wpdb->query("DELETE FROM $dsp_temp_payments_table WHERE user_id = '$user_id'");
                $exist_membership_plan = $wpdb->get_row("SELECT * FROM $dsp_memberships_table where membership_id='$membership_plan_id'");
                $plan_days             = $exist_membership_plan->no_of_days;
                $wpdb->query("INSERT INTO $dsp_temp_payments_table SET user_id = '$user_id',plan_id = '$membership_plan_id',plan_amount ='$membership_plan_amount',plan_days='$plan_days',plan_name='$membership_plan',payment_date='$payment_date',start_date='$payment_date',expiration_date=DATE_ADD('$payment_date', INTERVAL $plan_days DAY),payment_status=0");
                $exist_gateway_address = $wpdb->get_row("SELECT * FROM $dsp_gateways_table");
                $business              = $exist_gateway_address->address;
                $currency_code         = $exist_gateway_address->currency;
                if (class_exists('Wpdating_Paypal_Public')) {
                    $wpdating_paypal_public = new Wpdating_Paypal_Public();
                    $append_name            = 'user_id';
                    $append_value           = $user_id;
                    $wpdating_paypal_public->get_custom_field_value();
                    $custom_field_val = $wpdating_paypal_public->append_values_to_custom_field($append_name, $append_value);
                }
                ?>
                <form name="frm1" action="<?php echo $root_link . "setting/dsp_paypal/"; ?>" method="post">
                    <input type="hidden" name="business" value="<?php echo $business ?>"/>
                    <input type="hidden" name="mode" value="<?php echo $upgrade_mode ?>"/>
                    <input type="hidden" name="currency_code" value="<?php echo $currency_code ?>"/>
                    <input type="hidden" name="item_name" value="<?php echo $membership_plan ?>"/>
                    <input type="hidden" name="item_number" value="<?php echo $user_id ?>"/>
                    <input type="hidden" name="amount" value="<?php echo $membership_plan_amount ?>"/>
                    <input type="hidden" name="code" id="code" value="<?php echo $discount_code; ?>"/>
                    <input type="Hidden" name="return" value="<?php echo $root_link . "setting/dsp_paypal/"; ?>">
                    <input type="hidden" name="notify_url"
                           value="<?php echo site_url() . '/?wpdating-api=WC_Gateway_Paypal'; ?>">
                    <input type="hidden" name="custom" value="<?php echo isset($custom_field_val) ? $custom_field_val : ''; ?>">
                </form>
                <script type="text/javascript">
                    document.frm1.submit();
                </script>
                <?php
            }
            ?>

            <div class="heading-submenu dsp-upgrade-heading">
                <strong>
                    <?php echo __('Purchase Account', 'wpdating'); ?>
                </strong>
                    <p>
                        Purchase one of the below membership plans for family and friends as a gift!
                    </p>
            </div>

            <?php
            $exists_memberships_plan = $wpdb->get_results("SELECT * FROM $dsp_memberships_table where display_status='Y' AND free_plan = '0' ORDER BY date_added DESC");

            foreach ($exists_memberships_plan as $membership_plan) {
                $price                               = $membership_plan->price;
                $no_of_days                          = $membership_plan->no_of_days;
                $name                                = $membership_plan->name;
                $membership_id                       = $membership_plan->membership_id;
                $desc                                = $membership_plan->description;
                $image                               = $membership_plan->image;
                $free_plan                           = $membership_plan->free_plan;
                // $membership_stripe_recurring_plan_id = $membership_plan->stripe_recurring_plan_id;
                ?>
                    <div class="dspdp-col-sm-4 dsp-sm-4 dspdp-text-center">
                        <div class="box-border dsp-upgrade-container">
                            <div class="box-pedding">
                                <div class="setting-page__disable dsp-member-upgrade-page">
                                    <ul class="dspdp-row dspdp-xs-text-center">
                                        <li>
                                            <div class="dspdp-spacer "><img class="dspdp-img-responsive dspdp-block-center"
                                                                            src='<?php echo $imagepath ?>/uploads/dsp_media/dsp_images/<?php echo $image; ?>'
                                                                            title="<?php echo $name ?>"
                                                                            alt="<?php echo $name ?>"/></div>
                                        </li>
                                        <li>
                                            <div class="dspdp-spacer dspdp-upgrade-desc"><strong><?php echo $name; ?></strong></div>
                                        </li>
                                        <li>
                                            <div class="dspdp-spacer dspdp-upgrade-desc"><?php echo $desc; ?></div>
                                        </li>
                                        <li>
                                            <?php
                                            if ($check_gateways_mode->setting_status == 'Y') {
                                                $gateway_table = $wpdb->get_results("SELECT * FROM $dsp_gateways_table");

                                                foreach ($gateway_table as $gateway) {
                                                    if ($gateway->gateway_name == 'paypal' && $gateway->status == 1) {
                                                        ?>
                                                        <div>
                                                            <form name="paymentfrm"
                                                                  action="<?php echo $root_link . "setting/purchase_account/mode/purchase/"; ?>"
                                                                  method="post">
                                                                <input type="hidden" name="item_name" id="item_name"
                                                                       value="<?php echo $name; ?>"/>

                                                                <input type="hidden" name="amount" id="amount"
                                                                       value="<?php echo $price; ?>"/>

                                                                <input type="hidden" name="membership_id" id="membership_id"
                                                                       value="<?php echo $membership_id; ?>"/>


                                                                <input name="upgrade" title="Upgrade / PayPal" type="submit"
                                                                       value="<?php echo __('Purchase / PayPal', 'wpdating') ?>"
                                                                       class="dsp_span_pointer dspdp-btn dspdp-btn-default"
                                                                       style="text-decoration:none;"/>
                                                                <br/> <span
                                                                        style="font-size:130%; "><?php echo $gateway->currency_symbol; ?><?php echo $price ?></span>
                                                                <br/>
                                                            </form>
                                                        </div>
                                                        <?php
                                                    } 
                                                } // end of for each loop

                                                do_action('dsp_payment_addons', $user_id, $membership_id, $name, $price,
                                                    $no_of_days, $desc, $image, $blogInfo, $uploadInfo,
                                                    $membership_stripe_recurring_plan_id);
                                            } //if($check_gateways_mode->setting_status == 'Y'){
                                            else {
                                                echo "<div class='dsp_tab1-active' style='font-size:130%; background: '> Please contact Admin for purchasing the membership plan</div>";
                                            } // else if($check_gateways_mode->setting_status == 'N'){
                                            ?>
                                        </li>
                                    </ul>

                                </div>
                            </div>
                        </div>
                    </div>
                
            <?php } ?>
            <?php if (dsp_check_discount_mode()) { ?>
                <?php //include("wp-content/plugins/dsp_dating/dsp_discount_mode.php"); ?>
            <?php } ?>

            <script type="text/javascript">
                function payment(item_name, amount, id) {
                    // alert(' paymanet  ' + item_name + ' ' + amount + ' ' + id);
                    document.paymentfrm.item_name.value = item_name;
                    document.paymentfrm.amount.value = amount;
                    document.paymentfrm.membership_id.value = id;
//document.paymentfrm.submit();
                }
            </script>
            <?php
            //-------------------------------END UPGRADE ACCOUNT SETTINGS ---------------------------------- ?></div></div></div>