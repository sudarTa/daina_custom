<?php
$current_user			= get_current_user_id();
$dsp_categories_table = $wpdb->prefix . 'love_survey_categories';
$dsp_categories_setup_table = $wpdb->prefix . 'love_survey_categories_setup';
$dsp_survey_question_options_table = $wpdb->prefix . 'love_survey_question_options';

$dsp_survey_ques_details_table = $wpdb->prefix . 'love_survey_question_details';


$categories 		  = $wpdb->get_results("SELECT * FROM $dsp_categories_table");
// $wpdb->get_results("SELECT * FROM $dsp_categories_table");

if (isset($_POST['submit'])) {

$question_option_id         = isset($_REQUEST['option_id']) ? $_REQUEST['option_id'] : '';
foreach ($question_option_id as $key => $value) {
	if ($value != 0) {
		$num_rows1 = $wpdb->get_var("SELECT COUNT(*) FROM $dsp_survey_ques_details_table WHERE user_id=$current_user AND survey_question_id = $key" );
$rating         = isset($_REQUEST['rating_'.$key]) ? $_REQUEST['rating_'.$key] : '0';
		if ($num_rows1 > 0) {
	                    $wpdb->query("DELETE FROM $dsp_survey_ques_details_table where user_id = $current_user AND survey_question_id = $key");
	                }
	    $fetch_question_id = $wpdb->get_row("SELECT * FROM $dsp_survey_question_options_table WHERE ques_option_id = '" . $value . "'");

	    $wpdb->query("INSERT INTO $dsp_survey_ques_details_table SET user_id = $current_user, 	survey_question_id  = '$key', 	survey_question_option_id='" . $value . "',option_value='" . esc_sql($fetch_question_id->option_value) . "', rating = $rating");
    }
}
}
?>
<div class="box-border">
	<div class="box-pedding">
		<div class="heading-submenu dsp-block"
                 style="display:none"><?php echo __('Survey Questions', 'wpdating') ?></div>
	</div>
</div>
<div class="box-border profile-edit-page magn-top-15">
	<div class="box-pedding dsp-form-container form-horizontal">
		<div class="heading-submenu dsp-none"><?php echo __('Survey Questions', 'wpdating') ?></div>
		<div class="dsp-box-container">
            <div class="tab">
            	<?php
            	foreach ($categories as $category ) {
    			?>
    				<button class="tablinks" onclick="openCity(event, '<?php echo $category->name;?>')"><?php echo substr($category->name,0,20);?></button>
    			<?php
            	}
            	?>
			</div>
				<?php
            	foreach ($categories as $category ) {
    			?>
    				<div id="<?php echo $category->name;?>" class="tabcontent" <?php if ($category->categories_id !=1) { ?>style="display:none" <?php } ?>>
					  <h3><?php echo $category->name;?></h3>
					  	<form name="frm_survey_profile" id="frm_survey_profile" action="" method="post" enctype="multipart/form-data" class="dspdp-form-horizontal">
					  	<?php do_action('dsp_display_survey_question_by_order',$category->categories_id);?>
							<div class="dsp-button-container dsp-block" style="display:none">
		                		<input type="hidden" name="mode" value="update"/>
		                		<input type="submit" name="submit" class="dsp_submit_button dspdp-btn dspdp-btn-default"
		                       value="<?php _e(__('Save', 'wpdating')); ?>"/></div>
					  </form>
					</div>
    			<?php
    			}
    			?>
		</div>
	</div>
</div>
<script type="text/javascript">
	function openCity(evt, cityName) {
  // Declare all variables
  var i, tabcontent, tablinks;

  // Get all elements with class="tabcontent" and hide them
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }

  // Get all elements with class="tablinks" and remove the class "active"
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }

  // Show the current tab, and add an "active" class to the button that opened the tab
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
} 

function change(id,$ques_id) {
  // var value=document.getElementById(id).className;
  console.log("rating_"+$ques_id);
  rating_value = document.getElementById("rating_"+$ques_id);
  rating_value.value=id;

  var end=parseInt(id);
  for(var i=1; i<=end; i++)
      {
        console.log('rating_'+i+'_'+$ques_id);
         document.getElementById('rating_'+i+'_'+$ques_id).classList.remove('fa-star-o');
         document.getElementById('rating_'+i+'_'+$ques_id).classList.add('fa-star');

      }
  for(var i=5; i>=end+1; i--)
      {
         document.getElementById('rating_'+i+'_'+$ques_id).classList.remove('fa-star');
         document.getElementById('rating_'+i+'_'+$ques_id).classList.add('fa-star-o');
      }
  }

</script>