<?php
if ( !is_user_logged_in() ) 
	die();
$user_id = get_current_user_id();
$ref = get_user_meta( $user_id, 'ref_id', true );
if ( empty( $ref ) ) {
		$ref = md5($user_id);
		update_user_meta( $user_id, 'ref_id', $ref );
	}
// var_dump($ref);
?>
<div class="heading-submenu dsp-block" style="display:none"><?php echo __('Refer to friend', 'wpdating') ?></div>
<h4>Share the following link to refer family and friends to become part of the Blind Fate Dating community and get a referral bonus!
</h4>
<div class="box-border">
    <div class="box-pedding">
    	<p class=" dspdp-col-sm-6 dsp-sm-6">
    		<input type="text" class="dspdp-form-control dsp-form-control" id="Referal_link" value="<?php echo ROOT_LINK.'register/?ref='.$ref;?>"/>
    	</p>
    	<p class=" dspdp-col-sm-6 dsp-sm-6">
			<button class="dsp_submit_button dspdp-btn-default btn" onclick="copy()">Copy</button>
		</p>
    </div>
</div>
<script>
function copy() {
  var copyText = document.getElementById("Referal_link");
  copyText.select();
  copyText.setSelectionRange(0, 99999)
  document.execCommand("copy");
}
</script>