<?php
//For min and max age
$check_min_age = $wpdb->get_row( "SELECT * FROM $dsp_general_settings_table WHERE setting_name = 'min_age'");
$check_max_age = $wpdb->get_row( "SELECT * FROM $dsp_general_settings_table WHERE setting_name = 'max_age'");
$min_age_value = $check_min_age->setting_value;
$max_age_value = $check_max_age->setting_value;

?>
<div class="box-border"> 
   <div class="box-pedding">

   <form name="frm_zodiac_code_search" method="GET" action="<?php echo $root_link . "search/zodiac_search_result"; ?>"  class="dspdp-form-horizontal">
        <div class="heading-submenu">
            <strong>
                <?php echo __('Zodiac Search', 'wpdating'); ?>
            </strong>
        </div>
        <div class=" dsp-box-container dsp-space margin-btm-3 dsp-form-container">
            <?php //---------------------------------START  GENERAL SEARCH--------------------------------------- ?>

            <div class="box-pedding box-border ">
                <div class=" margin-btm-3">
                    <ul class="zip-search">
                        <li class="dspdp-form-group dsp-form-group">
                            <span class="dspdp-col-sm-3 dsp-sm-3 dspdp-control-label dsp-control-label">
                                <?php echo __('Seeking a:', 'wpdating') ?>
                            </span>
                            <span class="dspdp-col-sm-5 dsp-sm-5">
                                <select name="seeking" class="dspdp-form-control dsp-form-control">
                                    <option value="all" <?php if ($gender == 'all') { ?> selected="selected" <?php } else { ?> selected="selected"<?php } ?> >All</option>
                                    <?php   
                                            $gender = $userProfileDetailsExist ? $userProfileDetails->gender : '';
                                            echo get_gender_list($gender); 
                                    ?>
                                </select>
                            </span>
                        </li>
                        <li class="dspdp-form-group dsp-form-group">
                            <span class="dspdp-col-sm-3 dsp-sm-3 dspdp-control-label dsp-control-label">
                                <?php echo __('Age:', 'wpdating') ?>
                            </span> 
                            <span class="dspdp-col-sm-2 dsp-sm-2 dspdp-xs-form-group">
                                <select name="age_from" class="dspdp-form-control dsp-form-control">
                                    <?php
                                    for ($fromyear = $min_age_value; $fromyear <= $max_age_value; $fromyear++) {
                                        if ($fromyear == $min_age_value) {
                                            ?>
                                            <option value="<?php echo $fromyear ?>" selected="selected"><?php echo $fromyear ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $fromyear ?>"><?php echo $fromyear ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </span>
                            <span class="dspdp-col-sm-1 dsp-sm-1 dspdp-control-label dsp-control-label">
                                <?php echo __('to:', 'wpdating') ?>
                            </span> 			
                            <span  class="dspdp-col-sm-2 dsp-sm-2">
                                <select  name="age_to" class="dspdp-form-control dsp-form-control">
                                    <?php
                                    for ($fromyear = $max_age_value; $fromyear >= $min_age_value; $fromyear--) {
                                        if ($fromyear == $max_age_value) {
                                            ?>
                                            <option value="<?php echo $fromyear ?>" selected="selected"><?php echo $fromyear ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $fromyear ?>"><?php echo $fromyear ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </span>
                        </li>
                        <li class="dspdp-form-group dsp-form-group">
                            <span class="dspdp-col-sm-3 dsp-sm-3 dspdp-control-label dsp-control-label">
                                <?php echo __('Zodiac:', 'wpdating') ?>
                            </span>
                            <span class="dspdp-col-sm-5 dsp-sm-5">
                                <select name="zodiac" class="dspdp-form-control dsp-form-control">
                                    <option value="Aries">Aries</option>
                                    <option value="Taurus">Taurus</option>
                                    <option value="Gemini">Gemini</option>
                                    <option value="Cancer">Cancer</option>
                                    <option value="Leo">Leo</option>
                                    <option value="Virgo">Virgo</option>
                                    <option value="Libra">Libra</option>
                                    <option value="Scorpio">Scorpio</option>
                                    <option value="Sagittarius">Sagittarius</option>
                                    <option value="Capricorn">Capricorn</option>
                                    <option value="Aquarius">Aquarius</option>
                                    <option value="Pisces">Pisces</option>

                                    <!-- <option value="all" 
                                    <?php 
                                    if ($gender == 'all') { ?> selected="selected" <?php
                                    } else { 
                                        ?>
                                         selected="selected"<?php } ?> >All</option>
                                    <?php   
                                            $gender = $userProfileDetailsExist ? $userProfileDetails->gender : '';
                                            echo get_gender_list($gender); 
                                    ?> -->
                                </select>
                            </span>
                        </li>
                    </ul>
                </div>
                <div class="search-page-zip lm-dst-search">
                    <ul>
                        <li>
                            <span class="dspdp-form-group dsp-form-group  dspdp-block">
                                <span  class="dspdp-col-sm-6 dspdp-col-sm-offset-3">
                                    <input type="submit" name="submit" class="dsp_submit_button dspdp-btn dspdp-btn-default" value="<?php echo __('Submit', 'wpdating'); ?>" />
                                </span>
                            </span>	
                        </li>
                    </ul>
                </div>        
            </div>
        </div>
    </form>
    </div>
    </div>



