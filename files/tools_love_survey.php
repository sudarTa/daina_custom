<?php 

global $wpdb;
$dsp_categories_table = $wpdb->prefix . 'love_survey_categories';
$request_url = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : array();

$dsp_action = isset($_REQUEST['mode']) ? $_REQUEST['mode'] : '';
$sort_order = isset($_REQUEST['sort_order']) ? $_REQUEST['sort_order'] : '';
$categories_name = isset($_REQUEST['categories_name']) ? $_REQUEST['categories_name'] : '';

switch ($dsp_action) {
    case 'add':
    if (!empty($categories_name)) {
        $wpdb->query($wpdb->prepare("INSERT INTO $dsp_categories_table (name,sort_order) VALUES(%s,%d)", $categories_name, $sort_order));
    }
        break;
    
    default:
        break;
}
?>
<div style="height:8px;"></div>
<div>
	<div id="general" class="postbox">
		<h3 class="hndle"><?php echo __('Categories:', 'wpdating'); ?></h3>
		<table cellpadding="6" cellspacing="0" border="0" style="padding-left:20px;">
			<tr height="10%">
                <td><h4><?php echo __('Name:', 'wpdating'); ?></h4></td>
                <td width="100px">&nbsp;</td>
                <td><h4><?php echo __('Type:', 'wpdating'); ?></h4></td>
                <td width="40px">&nbsp;</td>
                <td><h4><?php echo __('Order:', 'wpdating'); ?></h4></td>
                <td width="40px">&nbsp;</td>
                <!-- <td><h4><?php echo __('Show Into Advanced Search', 'wpdating'); ?></h4></td> -->
                <td width="40px">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <?php 
            	$myrows = $wpdb->get_results("SELECT * FROM $dsp_categories_table Order by sort_order ASC");

            	foreach ($myrows as $categories) {
            		$categories_id = $categories->categories_id;
            		$name 		   = $categories->name;
            ?>
            <tr style="height: 25px;">
            	<td>
            		<a href="<?php echo add_query_arg(array('pid' => 'tools_love_survey_questions', 'categories_id' => $categories_id), $request_url); ?>">
                            <?php echo $name; ?>
                            </a>
                </td>
                <td width="100px">&nbsp;</td>
                <td></td>
                <td width="100px">&nbsp;</td>
                <td><?php echo $categories->sort_order; ?></td>
                <td width="40px">&nbsp;</td>
            </tr>
            <?php
            	}
            ?>
            <tr>
                <td height="50px;">&nbsp;</td>
            </tr>
        </table>
	</div>
</div>
<!-- Add section -->
<div class="profile_headind"><?php echo __('Add New Categories:', 'wpdating'); ?></div>
<div style="height:8px;"></div>
<div>
    <?php
    if (isset($_GET['Action']) && $_GET['Action'] == 'update') {
        $mode = 'update';
        $profile_option_id = array_key_exists('opt_Id',$_GET)?$_GET['opt_Id']: '';
    } else {
        $profile_option_id = 0;
        $mode = 'add';
    }?>

    <form name="frmaddoptions" method="post">
        <table cellpadding="0" cellspacing="0" border="0"  class="widefat">
            <tr>
                <td class="dsp_admin_headings2">
                    Categories:  
                </td>
                <td>
                    <input type="text" name="categories_name" size="30" value=""/>
                </td>
            </tr>
            <tr>
                <td class="dsp_admin_headings2">
                    Sort Order:  
                </td>
                <td>
                    <input type="text" name="sort_order" size="30" value="<?php if (isset($_REQUEST['Action']) && $_REQUEST['Action'] == 'update') {if(isset($dsp_updates) && !empty($dsp_updates)) echo $dsp_updates->sort_order;}?>"/>
                </td>
            </tr>
            <tr>
                <td width="40px"><input type="hidden" name="mode" value="<?php echo $mode ?>" /></td>
                <td><input type="button" name="submit1" class="button" value="<?php echo __('Add', 'wpdating'); ?>" onclick="add_survey_question_option();"/></td>
            </tr>
        </table>
    </form>
</div>
